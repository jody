#ifndef __SURFACE_H__
#define __SURFACE_H__

#include <SDL/SDL.h>

extern struct SDL_Surface *load_image(const char *file_name);

#endif
